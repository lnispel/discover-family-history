class Message < ApplicationRecord
    belongs_to :client 
    before_save :downcase_email
    validates :name, presence: true, length: { maximum: 50 }
    validates :phone, length: { minimum: 10 },
                           format: { with: /[0-9._]/,
                                     message: 'Can only contain numbers.' }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }

    def incoming_message_email
        MessageMailer.incoming_message(self).deliver_now
    end
    
    def outgoing_message_email
        MessageMailer.outgoing_message(self).deliver_now
    end

    private
    
    # Converts email to all lower-case
    def downcase_email
      self.email.downcase!
    end
end
