# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170627061124) do

  create_table "bills", force: :cascade do |t|
    t.string   "name"
    t.decimal  "total"
    t.boolean  "sent"
    t.boolean  "paid"
    t.string   "comments"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_bills_on_client_id"
    t.index ["created_at"], name: "index_bills_on_created_at"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "relation"
    t.decimal  "amount_billed"
    t.string   "billing_status"
    t.string   "previous_work"
    t.boolean  "previous_client"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["name", "created_at"], name: "index_clients_on_name_and_created_at"
  end

  create_table "documents", force: :cascade do |t|
    t.string   "name"
    t.string   "tags"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "img"
    t.index ["created_at"], name: "index_documents_on_created_at"
  end

  create_table "messages", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "organization_type"
    t.string   "service_type"
    t.string   "message"
    t.boolean  "read",              default: false
    t.integer  "client_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["client_id"], name: "index_messages_on_client_id"
    t.index ["name", "created_at"], name: "index_messages_on_name_and_created_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "activation_digest"
    t.boolean  "admin"
    t.boolean  "activated"
    t.datetime "activated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
