class DocumentsController < ApplicationController
  def create
    @document = Document.new(document_params)
    if @document.save 
      flash[:success] = 'Document Uploaded!'
      redirect_to document_path(@document)
    else 
      flash[:danger] = @document.errors
      redirect_to request.referer || documents_path
    end
  end

  def edit
      @document = Document.find_by(id: params[:format])
  end

  def update
     @document = Document.find_by(id: params[:id])
    if @document.update_attributes(document_params)
      flash[:success] = 'Document successfully updated!'
      redirect_to documents_path
    else
      render 'edit'
    end
  end
  
  def new
    @document = Document.new
  end
  
  def index
    @documents = Document.paginate(page: params[:page])
  end

  def show
    params[:id] = params[:id] || params[:format]
    @document = Document.find_by(id: params[:id])
  end

  def destroy
    @document = Document.find_by(id: params[:id])
    @document.destroy
    flash[:success] = 'Document deleted'
    redirect_to documents_path
  end
  
  private
  
  def document_params
    params.require(:document).permit(:name,
                                     :tags,
                                     :client_id,
                                     :img)
  end
end
