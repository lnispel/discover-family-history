class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
      t.string :name
      t.string :tags
      t.integer :client_id

      t.timestamps
    end
    add_index :documents, [:created_at]
  end
end
