class Document < ApplicationRecord
    belongs_to :user
    has_many :imgs  ## Added association
    has_many :img_attributes, :through => :imgs
    accepts_nested_attributes_for :imgs
    mount_uploader :img, DocumentUploader, :mount_on => :img
    validate :img_size_validation
 
    private
    
    def img_size_validation
        errors[:img] << "should be less than 2MB" if img.size > 2.megabytes
    end
end
