# Preview all emails at http://localhost:3000/rails/mailers/message_mailer
class MessageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/message_mailer/incoming_message
  def incoming_message
    MessageMailer.incoming_message
  end

  # Preview this email at http://localhost:3000/rails/mailers/message_mailer/outgoing_message
  def outgoing_message
    MessageMailer.outgoing_message
  end

end
