class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :organization_type
      t.string :service_type
      t.string :message
      t.boolean :read, :default => false
      t.references :client, foreign_key: true

      t.timestamps
    end
    add_index :messages, [:name, :created_at]
  end
end
