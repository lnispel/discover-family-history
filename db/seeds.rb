User.create!(name:  'Gail Shaffer Blankenau',
             email: 'gblankenau@discoverfamilyhistory.com',
             password:              'foobar',
             password_confirmation: 'foobar',
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  'Luke Nispel',
             email: 'lnispel@me.com',
             password:              'foobar',
             password_confirmation: 'foobar',
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
Client.create!(name: 'Bobby Joe',
               email: 'bobbyjoe@joey.com',
               phone: '5555555555')
               
Client.create!(name: 'Randy Olson',
               email: 'randyroe@roey.com',
               phone: '5555555556')
               
Message.create!(name: 'Randy Olson', 
                phone: '5555555556',
                email: 'randyroe@roey.com',
                client_id: 2,
                read: false,
                message: 'Hey Gail! Just looking for some gene splicing!')

Bill.create!(total: 3000.45,
             client_id: 1,
             sent: true,
             paid: true,
             comments: 'Bobby wanted family history investigation done!')