Rails.application.routes.draw do
  get 'documents/create'
  get 'documents/edit'
  get 'documents/update'
  get 'documents/show'
  get 'documents/new'
  get 'documents/destroy'
  get 'clients/new'
  get 'clients/edit'
  get 'clients/index'
  get 'clients/show'
  get 'clients/destroy'
  get 'bills/new'
  get 'bills/edit'
  get 'bills/update'
  get 'bills/show'
  get 'bills/destroy'
  get 'messages/create'
  get 'messages/new'
  get 'messages/edit'
  get 'messages/update'
  get 'messages/show'
  get 'messages/destroy'

  get '/signup',    to: 'users#new'
  post '/signup',   to: 'users#create'
  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'password_resets/edit'
  get 'password_resets/new'
  get 'sessions/new'
  get 'users/new'

  root 'static_pages#home'
  get '/home',       to: 'static_pages#home'
  get '/about',      to: 'static_pages#about'
  get '/resume',     to: 'static_pages#resume'
  get '/services',   to: 'static_pages#services'
  get '/contact',    to: 'static_pages#contact'
  resources :messages
  resources :documents
  resources :clients
  resources :bills
  resources :users do
    member do
      get :messages
      get :clients
      get :bills
      get :documents
    end
    resources :messages 
    resources :clients
    resources :bills
    resources :documents
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
end
