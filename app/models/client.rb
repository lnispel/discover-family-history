class Client < ApplicationRecord
    has_many :bills, dependent: :destroy 
    has_many :messages, dependent: :destroy 
    validates :email, uniqueness: true
end
