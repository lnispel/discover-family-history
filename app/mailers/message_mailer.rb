class MessageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message_mailer.incoming_message.subject
  #
  def incoming_message(message)
    @message = message
    mail to: "lnispel@me.com"
    mail from: @message.email
  end
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message_mailer.outgoing_message.subject
  #
  def outgoing_message(message)
    @message = message
    mail to: "to@example.org"
  end
end
