class AddImgToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :img, :string
  end
end
