class SessionsController < ApplicationController
  def new; end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      active
    else
      flash.now[:danger] = 'invalid email/password combination'
      render 'new'
    end
  end

  def active
    if @user.activated?
      start
    else
      check
    end
  end

  def start
    log_in @user
    params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
    redirect_back_or @user
  end

  def check
    message  = 'Account not activated. '
    message += 'Check your email for the activation link.'
    flash[:warning] = message
    redirect_to home_path
  end

  def destroy
    log_out if logged_in?
    redirect_to home_path
  end
end
