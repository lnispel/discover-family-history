class BillsController < ApplicationController
  def create
    @bill = Bill.new(bill_params)
    if @bill.save 
      flash[:success] = 'Bill Created!'
      redirect_to bill_path(@bill.client_id)
    else 
      redirect_to request.referer || bills_path
    end
  end
  
  def new
    @bill = Bill.new
  end

  def edit
    @bill = Bill.find_by(id: params[:format])
  end
  
  def update
    @bill = Bill.find_by(id: params[:id])
    if @bill.update_attributes(bill_params)
      flash[:success] = 'Bill updated!'
      redirect_to bills_show_path(@bill.client_id)
    else
      render 'edit'
    end
  end

  def show
    params[:id] = params[:id] || params[:format]
    @client = Client.find_by(id: params[:id])
  end

  def destroy
    @bill = Bill.find_by(id: params[:id])
    @bill.destroy
    flash[:success] = 'Bill deleted'
    redirect_to request.referer || bills_path
  end
  
  def index
    @bills = Bill.where(paid: false).paginate(page: params[:page])
    @billsAll = Bill.paginate(page: params[:page])
  end
  
  private 
  
  def bill_params
    params.require(:bill).permit(:total,
                                 :sent,
                                 :paid,
                                 :client_id,
                                 :comments)
  end
end
