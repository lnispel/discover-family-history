ruby '2.3.0'
source 'https://rubygems.org'

gem 'active_model_serializers',  '0.9.3'
gem 'bcrypt',                    '3.1.11'
gem 'bootstrap-sass',            '3.3.6'
gem 'bootstrap-will_paginate',   '0.0.10'
gem 'coffee-rails',              '4.2.1'
gem 'carrierwave',               '1.1.0',
     github: 'carrierwaveuploader/carrierwave'
gem 'daemons',                   '1.2.4'
gem 'delayed_job_active_record', '4.1.1'
gem 'faker',                     '1.6.6'
gem 'figaro',                    '1.1.1'
gem 'filterrific',               '2.1.2'
gem 'fog',                       '1.40.0'
gem 'image_magick',              '0.1.9'
gem 'jbuilder',                  '2.4.1'
gem 'jquery-rails',              '4.1.1'
gem 'puma',                      '3.4.0'
gem 'mime-types',                '3.1.0'
gem 'mini_magick',               '4.7.2'
gem 'rails',                     '5.0.1'
gem 'responders',                '2.4.0'
gem 'rubocop',                   '0.48.1', require: false
gem 'rubocop-rails',             '0.2.1'
gem 'sass-rails',                '5.0.6'
gem 'sdoc',                      '0.4.0', group: :doc
gem 'therubyracer',              '0.12.3', platforms: :ruby
gem 'turbolinks',                '5.0.1'
gem 'twilio-ruby',               '4.13.0'
gem 'twitter-bootstrap-rails',   '4.0.0', git:
    'git://github.com/seyhunak/twitter-bootstrap-rails.git'
gem 'uglifier',                  '3.0.0'
gem 'unf',                       '0.1.4'
gem 'validates_timeliness',      '4.0.0'
gem 'will_paginate',             '3.1.0'
gem 'workless',                  '1.2.2'

group :development, :test do
  gem 'byebug',  '9.0.0', platform: :mri
  gem 'sqlite3', '1.3.13'
end

group :development do
  gem 'listen',                '3.0.8'
  gem 'spring',                '1.7.2'
  gem 'spring-watcher-listen', '2.0.0'
  gem 'web-console',           '3.1.1'
end

group :test do
  gem 'guard',                    '2.13.0'
  gem 'guard-minitest',           '2.4.4'
  gem 'minitest-reporters',       '1.1.9'
  gem 'rails-controller-testing', '0.1.1'
end

group :production do
  gem 'pg', '0.20.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
