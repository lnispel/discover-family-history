class DocumentSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :image,
    :small,
    :medium,
    :large
  )

  def small
    url(width: 116, height: 116, crop: "fill")
  end

  def medium
    url(width: 640, height: 400, crop: "fill")
  end

  def large
    url(width: 1000, crop: "fill")
  end

  private

  def url(options={})
    options[:format] = "png"
    options[:secure] = true

    object.image_url(options)
  end
end
