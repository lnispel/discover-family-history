class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :relation
      t.decimal :amount_billed
      t.string :billing_status
      t.string :previous_work
      t.boolean :previous_client

      t.timestamps
    end
    add_index :clients, [:name, :created_at]
  end
end
