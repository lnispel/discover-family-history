class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.string :name
      t.decimal :total
      t.boolean :sent
      t.boolean :paid
      t.string :comments
      t.references :client, foreign_key: true

      t.timestamps
    end
    add_index :bills, [:created_at]
  end
end
