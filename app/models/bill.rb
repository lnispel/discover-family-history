class Bill < ApplicationRecord
    belongs_to :client 
    validates :total, presence: true
end
