class StaticPagesController < ApplicationController
  def enter; end
    
  def home; end

  def about; end

  def resume; end

  def services; end

  def contact
    if !logged_in?
      @message = Message.new
    else
      redirect_to messages_path
    end
  end
end
