class MessagesController < ApplicationController
  before_action :admin_user,     only: :destroy
  
  def create
    if !logged_in?
      check_client
      @message = @client.messages.new(message_params)
      message_save
    else 
      @client = Client.find_by(id: params[:message][:client_id])
      @reply = @client.messages.new(message_params)
      reply_save
    end
  end
  
  def check_client
    if previous_client
      @client = Client.find_by(email: message_params[:email])
    else
      @client = Client.create(client_params)
    end
  end
  
  def message_save
    if @message.save
      @message.incoming_message_email
      flash[:success] = 'Message Sent!'
      redirect_to contact_path
    else
      render 'static_pages/home'
    end
  end
  
  def reply_save
    if @reply.save
      @reply.outgoing_message_email
      flash[:success] = 'Message Sent!'
      redirect_to client_path(@client)
    else
      redirect_to request.referer || messages_path
    end
  end
  
  def index 
    if logged_in? 
      @messages = Message.where.not(read: true, email: current_user.email).paginate(page: params[:page])
      @messagesAll = Message.where.not(email: current_user.email).paginate(page: params[:page])
    else 
      redirect_to(home_path)
    end
  end

  def edit; end

  def update
     @message = Message.find_by(id: params[:id])
    if @message.read == false
      @message.update_attributes(:read => true)
    else 
      @message.update_attributes(:read => false)
    end
    redirect_to messages_path
  end
  
  def new
    @reply = Message.new
  end

  def show
    params[:id] = params[:id] || params[:format]
    @client = Client.find_by(id: params[:id])
    @messages = @client.messages
    @messages.update_all(read: true)
    @reply = Message.new
  end

  def destroy
    @message = Message.find_by(id: params[:id])
    @message.destroy
    flash[:success] = 'Message deleted'
    redirect_to request.referer || messages_path
  end
  
  def previous_client
    Client.find_by(email: message_params[:email])
  end

  private
  
  def message_params
    params.require(:message).permit(:name, 
                                    :phone,
                                    :email,
                                    :client_id,
                                    :organization_type,
                                    :service_type,
                                    :message)
  end
  
  def client_params
    params.require(:message).permit(:name, 
                                    :phone,
                                    :email)
  end
  
  def admin_user
    redirect_to(home_path) unless current_user.admin?
  end
end
