class ClientsController < ApplicationController
  def create
    @client = Client.new(client_params)
    if @client.save 
      flash[:success] = 'Client Created!'
      redirect_to client_path(@client)
    else 
      redirect_to request.referer || clients_path
    end
  end
  
  def new 
    @client = Client.new
  end
  
  def index
    @clients = Client.paginate(page: params[:page])
  end

  def edit
    @client = Client.find_by(id: params[:format])
  end

  def update
    @client = Client.find_by(id: params[:id])
    if @client.update_attributes(client_params)
      flash[:success] = 'Client successfully updated!'
      redirect_to @client
    else
      render 'edit'
    end
  end

  def show
    @client = Client.find_by(id: params[:id])
    @billTotal = @client.bills.where(paid: false).sum(:total)
    @messages = @client.messages.paginate(page: params[:page])
    @reply = Message.new
  end

  def destroy
    @client = Client.find_by(id: params[:id])
    @client.destroy
    flash[:success] = 'Client deleted'
    redirect_to request.referer || clients_path
  end
  
  private
  
  def client_params
    params.require(:client).permit(:name, 
                                    :phone,
                                    :email,
                                    :relation,
                                    :billing_status,
                                    :amount_billed,
                                    :previous_work)
  end
end
