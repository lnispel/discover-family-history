# Discover Family History Rails Website Development 

# Built using Cloud9 IDE and Bitbucket 

https://bitbucket.org/lnispel/discover-family-history
http://discoverfamilyhistory.com/

Built by Luke Nispel- 2017

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```