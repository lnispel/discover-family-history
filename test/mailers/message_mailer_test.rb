require 'test_helper'

class MessageMailerTest < ActionMailer::TestCase
  test "incoming_message" do
    mail = MessageMailer.incoming_message
    assert_equal "Incoming message", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "outgoing_message" do
    mail = MessageMailer.outgoing_message
    assert_equal "Outgoing message", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
